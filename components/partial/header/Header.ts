import { Component, Vue } from 'vue-property-decorator';

import MagnifyIcon from 'icons/Magnify.vue';
import HeaderDropdown from '~/components/partial/header/header-dropdown/HeaderDropdown.vue';

@Component({
  components: {
    HeaderDropdown,
    MagnifyIcon
  }
})
export default class Header extends Vue {
  public isMobileSearchActive: boolean = false;
}
