import { Component, Vue } from 'vue-property-decorator';

import ChevronDown from 'icons/ChevronDown.vue';
import Dropdown from '~/components/dropdown/Dropdown.vue';

@Component({
  components: {
    ChevronDown,
    Dropdown
  }
})
export default class HeaderDropdown extends Vue {
  public isOpen: boolean = false;

  public onToggle(isOpened: boolean) {
    this.isOpen = isOpened;
  }

  public testClickOutside() {
    console.log('clicked outside');
  }
}
