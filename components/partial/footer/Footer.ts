import { Component, Vue } from 'vue-property-decorator';

import AppleIcon from 'icons/Apple.vue';
import AndroidDebugBridgeIcon from 'icons/AndroidDebugBridge.vue';
import FacebookIcon from 'icons/Facebook.vue';
import VkIcon from 'icons/Vk.vue';
import TwitterIcon from 'icons/Twitter.vue';
import OdnoklassnikiIcon from 'icons/Odnoklassniki.vue';
import InstagramIcon from 'icons/Instagram.vue';
import YoutubeIcon from 'icons/Youtube.vue';

@Component({
  components: {
    AndroidDebugBridgeIcon,
    AppleIcon,
    InstagramIcon,
    FacebookIcon,
    OdnoklassnikiIcon,
    TwitterIcon,
    VkIcon,
    YoutubeIcon
  }
})
export default class Footer extends Vue {
  public links = {
    about: [
      {
        name: 'О проекте',
        to: '/about'
      },
      {
        name: 'Правовая информация',
        to: '/about'
      },
      {
        name: 'Правообладателям',
        to: '/about'
      },
      {
        name: 'Помогите MyBook стать лучше',
        to: '/about'
      },
      {
        name: 'Сотрудничество с MyBook',
        to: '/about'
      }
    ],
    subscribe: [
      {
        name: 'Купить подписку',
        to: '/subscribe'
      },
      {
        name: 'Подарить подписку',
        to: '/subscribe'
      },
      {
        name: 'Как оплатить',
        to: '/subscribe'
      },
      {
        name: 'Ввести подарочный код',
        to: '/subscribe'
      },
      {
        name: 'Библиотека для компаний',
        to: '/subscribe'
      },
      {
        name: 'Помощь',
        to: '/subscribe'
      }
    ],
    additional: [
      {
        name: 'Все бесплатные книги',
        to: '/additional'
      },
      {
        name: 'Издать свою книгу',
        to: '/additional'
      },
      {
        name: 'MyBook: истории',
        to: '/additional'
      }
    ]
  };
}
