import { Component, Vue } from 'vue-property-decorator';
import HeadphonesIcon from 'icons/Headphones.vue';
import CategoryCard from '~/components/category-card/CategoryCard.vue';

@Component({
  components: {
    CategoryCard,
    HeadphonesIcon
  }
})
export default class CategoryList extends Vue {
  /**
   * Options for awesome-swiper plugin.
   */
  public swiperOptions = {
    slidesPerView: 'auto',
    freeMode: true,
    grabCursor: true
  };

  public categories = [
    {
      icon: '🎧',
      name: 'Аудиокниги',
      link: '/audiobooks'
    },
    {
      icon: '👌',
      name: 'Бесплатные книги',
      link: '/free-books'
    },
    {
      icon: '🔥',
      name: 'Новинки',
      link: '/new'
    },
    {
      icon: '❤️',
      name: 'Топ книг',
      link: '/top'
    },
    {
      icon: '🎙',
      name: 'Топ аудиокниг',
      link: '/top-audio'
    },
    {
      icon: '⚡',
      name: 'Истории',
      link: '/stories'
    }
  ];
}
