import { Component, Vue } from 'vue-property-decorator';

import BookshelfIcon from 'icons/Bookshelf.vue';
import HeadphonesIcon from 'icons/Headphones.vue';
import StarOutlineIcon from 'icons/StarOutline.vue';
import PuzzleOutlineIcon from 'icons/PuzzleOutline.vue';
import WifiOffIcon from 'icons/WifiOff.vue';

@Component({
  components: {
    BookshelfIcon,
    HeadphonesIcon,
    StarOutlineIcon,
    PuzzleOutlineIcon,
    WifiOffIcon
  }
})
export default class TopBlock extends Vue {}
