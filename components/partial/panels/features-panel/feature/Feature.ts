import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class Feature extends Vue {
  @Prop()
  public feature: {
    img: string;
    title: string;
    description: string;
  };

  @Prop({
    default: () => false
  })
  public isStatic: boolean;
}
