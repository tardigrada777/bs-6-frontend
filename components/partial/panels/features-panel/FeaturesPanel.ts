import { Component, Vue } from 'vue-property-decorator';
import Feature from '~/components/partial/panels/features-panel/feature/Feature.vue';

@Component({
  components: {
    Feature
  }
})
export default class FeaturesPanel extends Vue {
  public swiperOptions = {
    slidesPerView: 1,
    pagination: {
      el: '.swiper-pagination'
    }
  };

  public features: Array<{
    img: string;
    title: string;
    description: string;
  }> = [
    {
      img: '/img/girl-books.svg',
      title: 'Все книги доступны одновременно',
      description:
        'Пользуйтесь подпиской – безлимитным абонементом на весь каталог'
    },
    {
      img: '/img/girl-recomendations.svg',
      title: 'Персональные рекомендации',
      description:
        'Подбираем книги по вашему вкусу на основе того, что вы прочитали'
    },
    {
      img: '/img/girl-statistics.svg',
      title: 'Статистика чтения',
      description:
        'Показываем время, скорость и прогресс вашего чтения в приложении'
    }
  ];
}
