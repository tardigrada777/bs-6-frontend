import { Component, Vue } from 'vue-property-decorator';

import BooksetCard from '~/components/bookset-card/BooksetCard.vue';

@Component({
  components: {
    BooksetCard
  }
})
export default class MainBookset extends Vue {
  public bookset = [
    {
      cover: '/img/mock_sets/weekly_updates.jpg',
      title: 'Новинки недели',
      bookCount: 57,
      link: '/sets/weekly-updates'
    },
    {
      cover: '/img/mock_sets/chineese_books.jpeg',
      title: '"Лягушки" и другие книги китайских авторов',
      bookCount: 9,
      link: '/sets/weekly-updates'
    },
    {
      cover: '/img/mock_sets/pashalki.jpg',
      title: 'Пасхалки в книгах',
      bookCount: 16,
      link: '/sets/weekly-updates'
    },
    {
      cover: '/img/mock_sets/circum.jpeg',
      title: 'Магия цирка',
      bookCount: 20,
      link: '/sets/weekly-updates'
    },
    {
      cover: '/img/mock_sets/facts.png',
      title: 'По ту сторону обложки: самые интересные факты о книгах',
      bookCount: 15,
      link: '/sets/weekly-updates'
    },
    {
      cover: '/img/mock_sets/nazbest.jpg',
      title: 'Премия "Нацбест-2020": шорт-лист',
      bookCount: 5,
      link: '/sets/weekly-updates'
    },
    {
      cover: '/img/mock_sets/only_el.jpg',
      title: 'Только в электронном виде',
      bookCount: 7,
      link: '/sets/weekly-updates'
    },
    {
      cover: '/img/mock_sets/only_audio.jpeg',
      title: 'Только в аудио!',
      bookCount: 7,
      link: '/sets/weekly-updates'
    }
  ];

  /**
   * Options for awesome-swiper plugin.
   */
  public swiperOption = {
    slidesPerView: 'auto',
    freeMode: true,
    grabCursor: true
  };
}
