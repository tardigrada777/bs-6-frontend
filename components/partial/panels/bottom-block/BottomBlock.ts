import { Component, Vue } from 'vue-property-decorator';

import AndroidDebugBridgeIcon from 'icons/AndroidDebugBridge.vue';

@Component({
  components: {
    AndroidDebugBridgeIcon
  }
})
export default class BottomBlock extends Vue {}
