import { Component, Vue } from 'vue-property-decorator';
import ChevronRightIcon from 'icons/ChevronRight.vue';
import bestsellersBooks from 'static/mock-data/bestsellers.json';
import Panel from '~/components/panel/Panel.vue';
import Tag from '~/components/common/tag/Tag.vue';
import BookCard from '~/components/card/Card.vue';

@Component({
  components: {
    BookCard,
    ChevronRightIcon,
    Panel,
    Tag
  }
})
export default class BestsellersPanel extends Vue {
  /**
   * Options for awesome-swiper plugin.
   */
  public swiperOption = {
    slidesPerView: 'auto',
    freeMode: true,
    grabCursor: true
  };

  /**
   * Mock books for swiper.
   */
  public bestsellers = bestsellersBooks;
}
