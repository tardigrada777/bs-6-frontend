import { Component, Prop, Vue } from 'vue-property-decorator';
import ClampText from 'vue-clamp';

import ReadonlyRating from '~/components/readonly-rating/ReadonlyRating.vue';

@Component({
  components: {
    ClampText,
    ReadonlyRating
  }
})
export default class Card extends Vue {
  // TODO: add pro, free and audio indicators to book cover (?)

  /**
   * Book for displaying by this card.
   */
  @Prop({
    required: true
  })
  public book: CardBook;

  /**
   * Indicates that card used for audio book.
   */
  @Prop({
    default: () => false
  })
  public isAudio: boolean;

  /**
   * Alt property for cover of book.
   */
  public get imgAlt() {
    return `${this.book.author} - ${this.book.title}`;
  }
}

export interface CardBook {
  cover: string;
  author: string;
  title: string;
  link: string;
  rating: number;
}
