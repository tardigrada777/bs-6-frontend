import { Component, Prop, Vue } from 'vue-property-decorator';

import EyeIcon from 'icons/Eye.vue';

@Component({
  components: {
    EyeIcon
  }
})
export default class BaseInput extends Vue {
  public isPasswordRevealed: boolean = false;

  @Prop({
    default: () => null
  })
  public label: string | null;

  @Prop({
    default: () => false
  })
  public passwordReveal: boolean;

  public get inputType() {
    if (this.$attrs.type !== 'password') {
      return this.$attrs.type;
    }

    if (!this.passwordReveal) {
      return this.$attrs.type;
    }

    return this.isPasswordRevealed ? 'text' : 'password';
  }
}
