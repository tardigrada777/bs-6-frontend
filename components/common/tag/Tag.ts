import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class Tag extends Vue {
  /**
   * Text content of tag.
   */
  @Prop()
  public text: string;

  /**
   * Route of the page for this tag.
   */
  @Prop()
  public to: string;
}
