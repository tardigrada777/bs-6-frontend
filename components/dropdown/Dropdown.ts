import { Component, Vue } from 'vue-property-decorator';

@Component
export default class Dropdown extends Vue {
  public isOpen: boolean = false;

  public toggle() {
    this.isOpen = !this.isOpen;
    this.$emit('toggled', this.isOpen);
  }

  public close() {
    this.isOpen = false;
    this.$emit('toggled', this.isOpen);
  }
}
