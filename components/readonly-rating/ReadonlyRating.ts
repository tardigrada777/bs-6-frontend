import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class ReadonlyRating extends Vue {
  /**
   * Rating to display.
   */
  @Prop()
  public rating: number;
}
