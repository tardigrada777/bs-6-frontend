import { Component, Vue } from 'vue-property-decorator';

import CloseIcon from 'icons/Close.vue';
import FacebookIcon from 'icons/Facebook.vue';
import VkIcon from 'icons/Vk.vue';
import GoogleIcon from 'icons/Google.vue';
import OkIcon from 'icons/Odnoklassniki.vue';
import TwitterIcon from 'icons/Twitter.vue';
import BaseInput from '~/components/common/base-input/BaseInput.vue';
import { BodyScrollLock } from '~/plugins/BodyScrollLock';

export enum AuthModalState {
  LOGIN,
  REGISTER
}

@Component({
  components: {
    BaseInput,
    CloseIcon,
    GoogleIcon,
    FacebookIcon,
    OkIcon,
    TwitterIcon,
    VkIcon
  }
})
export default class AuthModal extends Vue {
  public stateTypes = AuthModalState;
  public currentState: AuthModalState = AuthModalState.LOGIN;
  public socialList: Array<{
    component: string;
    color: string;
  }> = [
    {
      color: 'facebook',
      component: 'facebook-icon'
    },
    {
      color: 'vk',
      component: 'vk-icon'
    },
    {
      color: 'google',
      component: 'google-icon'
    },
    {
      color: 'ok',
      component: 'ok-icon'
    },
    {
      color: 'twitter',
      component: 'twitter-icon'
    }
  ];

  readonly $refs: {
    modal: HTMLElement;
  };

  public beforeOpenHandler() {
    BodyScrollLock.disableScroll(this.$refs.modal);
  }

  public beforeCloseHandler() {
    BodyScrollLock.enableScroll(this.$refs.modal);
  }

  public setCurrentState(state: AuthModalState) {
    this.currentState = state;
  }
}
