import { Component, Vue } from 'vue-property-decorator';

import BooksIcon from 'icons/Bookshelf.vue';

@Component({
  layout: 'action',
  components: {
    BooksIcon
  }
})
export default class PaymentPage extends Vue {}
