import { Component, Vue } from 'vue-property-decorator';
import otherBooksFromSeries from 'static/mock-data/from-series.json';

import ArrowRightIcon from 'icons/ChevronRight.vue';
import ShareIcon from 'icons/ShareVariant.vue';
import BookOpenIcon from 'icons/BookOpen.vue';
import AndroidIcon from 'icons/AndroidDebugBridge.vue';
import AppleIcon from 'icons/Apple.vue';
import HeadphonesIcon from 'icons/Headphones.vue';
import Tag from '~/components/common/tag/Tag.vue';
import BookCard from '~/components/card/Card.vue';
import ReadonlyRating from '~/components/readonly-rating/ReadonlyRating.vue';

@Component({
  components: {
    AndroidIcon,
    AppleIcon,
    ArrowRightIcon,
    BookOpenIcon,
    BookCard,
    HeadphonesIcon,
    ReadonlyRating,
    ShareIcon,
    Tag
  }
})
export default class BookPage extends Vue {
  /**
   * Options for awesome-swiper plugin.
   */
  public swiperOption = {
    slidesPerView: 'auto',
    freeMode: true,
    grabCursor: true
  };

  public mockOtherBooksFromSeries = otherBooksFromSeries;
}
