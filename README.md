## Types of commits

1. __feat__: feature or improvement
2. __refactor__: refactoring
3. __fix__: bug fixing
4. __ci__: working on ci/cd
5. __tests__: working on tests
6. __docs__: working on documentation
7. __chore__: cleaning etc.

## Namespaces for commit messages

1. __ui__: all ui/ux stuff
2. __tools__: tools and software configurations
3. __core__: business logic
4. __api__: working on API related stuff
5. __<readme, contrib ...>__: README, CONTRIBUTING etc. files
