/*
 * TailwindCSS Configuration File
 *
 * Docs: https://tailwindcss.com/docs/configuration
 * Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    extend: {
      colors: {
        facebook: '#3b5998',
        vk: '#4d7299',
        google: '#DB3236',
        ok: '#ff983a',
        twitter: '#00abf8',
        litres: '#e7ebed'
      }
    }
  },
  variants: {},
  plugins: []
};
