// eslint-disable-next-line nuxt/no-cjs-in-config
const { resolve } = require('path');

export default {
  mode: 'universal',

  /*
   * Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }]
  },
  /*
   * Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   * Global CSS
   */
  css: [
    '~/assets/css/responsive.css',
    '~/assets/css/plugins.css',
    '~/assets/css/icons.css'
  ],
  /*
   * Plugins to load before mounting the App
   */
  plugins: ['~/plugins/Swiper', '~/plugins/ClickOutside', '~/plugins/Modal'],
  /*
   * Nuxt.js dev-modules
   */
  buildModules: ['@nuxt/typescript-build'],
  /*
   * Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/tailwindcss'
  ],
  /*
   * Axios module configuration
   * See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   * Build configuration
   */
  build: {
    /*
     * You can extend webpack config here
     */
    extend(config, ctx) {
      config.resolve.alias.icons = resolve(
        process.cwd(),
        'node_modules/vue-material-design-icons'
      );
    },
    transpile: ['vue-clamp', 'resize-detector']
  }
};
