import {
  disableBodyScroll,
  enableBodyScroll
  // @ts-ignore
} from 'body-scroll-lock/lib/bodyScrollLock.es6';

export class BodyScrollLock {
  /**
   * Block scrolling for all elements except the target.
   */
  public static disableScroll(target: HTMLElement) {
    disableBodyScroll(target);
  }

  /**
   * Unblock scrolling for all elements except the target.
   */
  public static enableScroll(target: HTMLElement) {
    enableBodyScroll(target);
  }
}
